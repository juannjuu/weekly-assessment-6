const filterArray = (arr) => {
    let result = []
    if(arr.length == 0) return []
    else{
        arr.forEach((e) => {
            if(typeof e === 'number'){
                if(result.includes(e)){
                    return 0
                }
                result.push(e)
            } 
        })
        return result
    }
}

console.log(filterArray([1, 2, "a", "b", 1, 2]))
console.log(filterArray([1, "a", "b", 0, 15]))
console.log(filterArray([1, 2, "aasf", "1", "123", 123]))
