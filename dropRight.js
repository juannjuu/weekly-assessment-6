const dropRight = (arr, n = 1) => {
    if(arr.length < n) return []
    else {
        arr.forEach((e, i) => {
            if(i < n) arr.pop()
        })
        return arr
    }
}

console.log(dropRight([1, 2, 3]))
console.log(dropRight([1, 2, 3], 2))
console.log(dropRight([1, 2, 3], 5))
console.log(dropRight([1, 2, 3], 0))