const sumOfCubes = (arr) =>{
    let result = 0
    if(arr.length == 0) return 0
    else{
        arr.forEach((e) => {
            result += Math.pow(e, 3)
        })
        return result
    }
}

console.log(sumOfCubes([1, 5, 9]))
console.log(sumOfCubes([3, 4, 5]))
console.log(sumOfCubes([2]))
console.log(sumOfCubes([]))